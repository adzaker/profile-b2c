const fs = require('fs');

async function getIcons() {
  const iconsFolder = './components/atoms/Icon/imgs/';
  const iconsArray = [];
  await fs.readdir(iconsFolder, (err, files) => {
    files.forEach((icon) => {
      if (icon.indexOf('.js') === -1) {
        iconsArray.push(`'${icon.slice(0, -4)}'`);
      }
    });
    fs.writeFile(`${iconsFolder}data.js`, `export default [${iconsArray}];`, (error) => {
      if (error) throw error;
    });
  });
}

getIcons();
