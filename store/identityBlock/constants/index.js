const ADD_PHOTO = 'ADD_PHOTO';
const ADD_MULTIPLE_PHOTOS = 'ADD_MULTIPLE_PHOTOS';
const REMOVE_PHOTO = 'REMOVE_PHOTO';

export const types = {
  ADD_PHOTO,
  ADD_MULTIPLE_PHOTOS,
  REMOVE_PHOTO,
};

export const identityInitials = {
  link: null,
  photos: [],
};
