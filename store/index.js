import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import personal from './personalBlock/reducer';
import passport from './passportBlock/reducer';
import identity from './identityBlock/reducer';
import asideMenu from './asideMenu/reducer';
import topWidget from './topWidget/reducer';

export const rootReducer = combineReducers({
  personal,
  passport,
  asideMenu,
  identity,
  topWidget,
  form: formReducer,
});

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  );
}
