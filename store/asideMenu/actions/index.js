import { types } from '../constants';

export function changeActiveNumber(number) {
  return {
    type: types.CHANGE_ACTIVE_NUMBER,
    number,
  };
}
