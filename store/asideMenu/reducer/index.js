import {
  asideMenuInitials, types,
} from '../constants';

export default function asideMenu(state = asideMenuInitials, action) {
  const newState = { ...state };
  switch (action.type) {
    case types.CHANGE_ACTIVE_NUMBER:
      newState.activeItem = Number(action.number);
      return newState;
    default:
      return newState;
  }
}
