const CHANGE_ACTIVE_NUMBER = 'CHANGE_ACTIVE_NUMBER';

export const types = {
  CHANGE_ACTIVE_NUMBER,
};

export const asideMenuInitials = {
  activeItem: 0,
  menuItems: [
    {
      name: 'personal',
      icon: 'menuPersonal',
      label: 'Личная информация',
      scrollTo: 'PersonalForm',
    },
    {
      name: 'documents',
      icon: 'menuDocuments',
      label: 'Загруженные документы',
      scrollTo: 'PassportForm',
    },
    {
      name: 'identity',
      icon: 'menuIdentity',
      label: 'Идентификация',
      scrollTo: 'IdentityForm',
    },
  ],
};

export const scrollingBlockOffset = 120;
