const LOAD_USER_PHOTO = 'LOAD_USER_PHOTO';
const REMEMBER_STATE = 'REMEMBER_STATE';
const GET_PROFILE_SETTINGS = 'GET_PROFILE_SETTINGS';

export const types = {
  LOAD_USER_PHOTO,
  REMEMBER_STATE,
  GET_PROFILE_SETTINGS,
};

export const personalInitials = {
  nickname: {
    name: '',
    buttons: [
      'change',
    ],
  },
  owner: 'ЗАО «Шестерочка»',
  surname: 'Денисов',
  name: 'Иван',
  lastname: 'Иванович',
  birthdate: '1997-02-12T00:00:00',
  gender: 'Мужской',
  inn: {
    name: '',
    buttons: [
      'change',
    ],
  },
  snils: {
    name: '',
    buttons: [
      'change',
    ],
  },
  phone: '9163456789',
  email: 'idenisov@gmail.com',
  profileStatus: 'Базовый',
  // photo: 'https://www.placecage.com/136/136',
};
