const OPEN_TOP_WIDGET = 'OPEN_TOP_WIDGET';
const CLOSE_TOP_WIDGET = 'CLOSE_TOP_WIDGET';
const TOGGLE_TOP_WIDGET = 'TOGGLE_TOP_WIDGET';

export const types = {
  OPEN_TOP_WIDGET,
  CLOSE_TOP_WIDGET,
  TOGGLE_TOP_WIDGET,
};

export const topWidgetInitials = {
  isShown: false,
  menuItems: [
    {
      name: 'manageAccount',
      icon: 'menuPersonal',
      label: 'Управление аккаунтом',
      url: '/',
    },
    {
      name: 'myDocuments',
      icon: 'menuDocuments',
      label: 'Мои документы',
      url: '/documents',
    },
    {
      name: 'circulation',
      icon: 'circulation',
      label: 'Обращения',
      url: '/circulation',
    },
    {
      name: 'exit',
      icon: 'exit',
      label: 'Выход',
      url: '/exit',
    },
  ],
};
