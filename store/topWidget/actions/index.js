import { types } from '../constants';

export function openTopWidget() {
  return {
    type: types.OPEN_TOP_WIDGET,
  };
}

export function closeTopWidget() {
  return {
    type: types.CLOSE_TOP_WIDGET,
  };
}

export function toggleTopWidget() {
  return {
    type: types.TOGGLE_TOP_WIDGET,
  };
}
