import {
  topWidgetInitials, types,
} from '../constants';

export default function topWidget(state = topWidgetInitials, action) {
  const newState = { ...state };
  switch (action.type) {
    case types.OPEN_TOP_WIDGET:
      newState.isShown = true;
      return newState;
    case types.CLOSE_TOP_WIDGET:
      newState.isShown = false;
      return newState;
    case types.TOGGLE_TOP_WIDGET:
      newState.isShown = !newState.isShown;
      return newState;
    default:
      return newState;
  }
}
