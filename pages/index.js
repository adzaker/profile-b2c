import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import fetch from 'isomorphic-unfetch';
import styled from 'styled-components';
import PersonalForm from '../components/organisms/PersonalForm';
import PassportForm from '../components/organisms/PassportForm';
import IdentityForm from '../components/organisms/IdentityForm';
import AsideMenu from '../components/organisms/AsideMenu';
import HorizontalSpacer from '../components/atoms/HorisontalSpacer';
import TopUserForm from '../components/organisms/TopUserBlock';
import Footer from '../components/organisms/Footer';
import Header from '../components/organisms/Header';
import TopWidget from '../components/organisms/TopWidget';
import { changeActiveNumber } from '../store/asideMenu/actions';
import { loadPhoto, rememberState, getProfileSettings } from '../store/personalBlock/actions';
import { scrollingBlockOffset } from '../store/asideMenu/constants';
import footerLinks from '../constants/footerLinks';
import TopWidgetImage from '../components/moleculas/TopWidgetImage';

const DetailPage = styled.section`
    width: 80rem;
    padding: 0 3.5rem;
    display: flex;
    margin: 0 auto;
    box-sizing: border-box;
    
    main {
      margin-left: 2rem;
      max-width: 54.25rem;
      width: 100%;
    }
`;

const TopUserBlock = styled.section`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 0 3.5rem;
  width: 80rem;
  box-sizing: border-box;
`;

const HomePage = () => {
  const personal = useSelector((state) => (state.personal));
  const passport = useSelector((state) => (state.passport));
  const asideMenu = useSelector((state) => (state.asideMenu));
  const { menuItems } = asideMenu;

  const dispatch = useDispatch();

  const [isDocksOpened, setDocksOpened] = useState(false);


  const toggleDocksOpened = () => {
    setDocksOpened(!isDocksOpened);
  };

  const submit = () => {
    console.log('submitted');
  };
  const changePhoto = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      dispatch(loadPhoto(reader.result));
    };

    reader.readAsDataURL(file);
  };

  const changeWindowScroll = (value) => {
    window.scrollBy({
      top: value - scrollingBlockOffset,
      behavior: 'smooth',
    });
  };

  const rememberChanges = (values) => {
    dispatch(rememberState(values));
  };

  const scrollToBlock = (e) => {
    const scrollBlock = window.document.getElementById(e.currentTarget.dataset.scroll);
    const scrollValue = scrollBlock.getBoundingClientRect().top;
    changeWindowScroll(scrollValue);
  };

  const year = (new Date()).getFullYear();

  const fetchProfileSettings = async () => {
    try {
      const response = await fetch('https://login.mts.ru/api/profile', {
        headers: {
          'Content-Type': 'application/json',
        },
        credentials: 'include',
      });
      const result = await response.json();
      dispatch(getProfileSettings(result));
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(`Ошибка: ${error}`);
    }
  };


  useEffect(() => {
    fetchProfileSettings();
    const forms = window.document.querySelectorAll('main form');
    window.addEventListener('scroll', () => {
      forms.forEach((form, index) => {
        const getRect = form.getBoundingClientRect();
        if (getRect.top <= scrollingBlockOffset
          && getRect.height + getRect.top >= scrollingBlockOffset
        ) {
          dispatch(changeActiveNumber(index));
        }
      });
    });
  }, []);

  return (
    <>
      <Header personal={personal}>
        <TopWidget />
      </Header>
      <HorizontalSpacer height="4.25rem" />
      <TopUserBlock>
        <HorizontalSpacer height="3.5rem" />
        <TopUserForm
          onSubmit={submit}
          values={personal}
          changePhoto={changePhoto}
          formID="TopUserForm"
        />
        <HorizontalSpacer height="2rem" />
      </TopUserBlock>
      <DetailPage>
        <AsideMenu
          items={menuItems}
          active={asideMenu.activeItem}
          scrollToBlock={scrollToBlock}
        />
        <main>
          <PersonalForm
            onSubmit={submit}
            defaultValues={personal}
            values={personal}
            formID="PersonalForm"
            rememberChanges={rememberChanges}
          />
          <HorizontalSpacer height=".25rem" />
          <PassportForm
            onSubmit={submit}
            defaultValues={personal}
            values={passport}
            isOpened={isDocksOpened}
            toggleOpened={toggleDocksOpened}
            formID="PassportForm"
          />
          <HorizontalSpacer height="3rem" />
          <IdentityForm
            onSubmit={submit}
            formID="IdentityForm"
          />
          <HorizontalSpacer height="3rem" />
        </main>
      </DetailPage>
      <Footer links={footerLinks} year={year} />
    </>
  );
};

export default HomePage;
//
// HomePage.getInitialProps = async function () {
//   const response = await fetch('http://localhost:3000/api/profile', {
//     headers: {
//       'Content-Type': 'application/json',
//       msisdn: '9175343564',
//     },
//     credentials: 'include',
//   });
//   console.log(response);
// };
