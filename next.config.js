const withFonts = require('next-fonts');
const withOptimizedImages = require('next-optimized-images');


const nextConfig = {
  webpack: (config, { dev }) => {
    return config;
  },
};

const optimizedImagesConfig = {
  inlineImageLimit: 8192,
  imagesFolder: 'images',
  imagesName: '[name]-[hash].[ext]',
  handleImages: ['jpeg', 'png', 'svg'],
  optimizeImages: true,
  optimizeImagesInDev: true,
  mozjpeg: {
    quality: 80,
  },
  optipng: {
    optimizationLevel: 3,
  },
  svgo: {
    plugins: [
      { removeComments: true },
    ],
  },
};

module.exports = withFonts(withOptimizedImages(optimizedImagesConfig, nextConfig));
