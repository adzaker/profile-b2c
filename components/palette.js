const lightBlue = '#0077DB';
const blue = '#0063B0';
const redMts = '#E30611';
const redDark = '#AD050D';
const textBlack = '#001424';
const bgGray = '#F2F3F7';
const white = '#fff';
const gray = '#BBC1C7';
const grayUltraLight = '#e2e5eb';
const grayRaven = '#6E7782';
const photoButtonBack = 'rgba(32, 45, 61, 0.8)';

const palette = {
  lightBlue,
  blue,
  redMts,
  textBlack,
  bgGray,
  white,
  grayRaven,
  photoButtonBack,
  gray,
  grayUltraLight,
  redDark,
};

export default palette;
