import React from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import UserInfo from '../../moleculas/UserInfo';
import UserPhoto from '../../moleculas/UserPhoto';


const FormContent = styled.div`
  display: flex;
`;

const TopUserForm = ({
  onSubmit,
  values,
  changePhoto,
}) => {
  const {
    register, handleSubmit, watch, errors,
  } = useForm({
    defaultValues: values,
  });
  return (
    <form onSubmit={onSubmit}>
      <FormContent>
        <UserPhoto
          image={values.photo}
          name={`${values.name} ${values.surname}`}
          changePhoto={changePhoto}
          register={register}
        />
        <UserInfo
          phone={values.phone}
          name={values.name}
          surname={values.surname}
          email={values.email}
          profileStatus={values.profileStatus}
        />
      </FormContent>
    </form>
  );
}


export default TopUserForm;

TopUserForm.propTypes = {
  values: PropTypes.objectOf(PropTypes.any).isRequired,
  onSubmit: PropTypes.func.isRequired,
  changePhoto: PropTypes.func.isRequired,
};
