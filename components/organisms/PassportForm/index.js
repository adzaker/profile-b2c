import React from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import CustomField from '../../moleculas/CustomField';
import FieldsRow from '../../moleculas/FieldsRow';
import TextHeader from '../../atoms/TextHeader';
import BlockHeader from '../../moleculas/BlockHeader';
import FieldsBlock from '../../moleculas/FieldsBlock';
import HorisontalSpacer from '../../atoms/HorisontalSpacer';
import FieldsToggleBlock from '../../moleculas/FieldsToggleBlock';
import FieldsCell from '../../moleculas/FieldsCell';


const PassportForm = ({
  onSubmit, values, formID, isOpened, toggleOpened,
}) => {
  const {
    register, handleSubmit, watch, errors,
  } = useForm({
    defaultValues: values,
  });

  return (
    <form onSubmit={onSubmit} id={formID}>
      <FieldsBlock>
        <BlockHeader isScrollable isOpened={isOpened} toggleOpened={toggleOpened}>
          <TextHeader header="Документ, удостоверяющий личность" />
        </BlockHeader>
        <FieldsToggleBlock isOpened={isOpened}>
          <FieldsRow>
            <FieldsCell lg={12}>
              <CustomField
                id="type"
                name="type"
                label="Тип документа"
                register={register}
                value={values.type}
              />
            </FieldsCell>
          </FieldsRow>
          <HorisontalSpacer />
          <FieldsRow>
            <FieldsCell lg={2}>
              <CustomField
                id="series"
                name="series"
                label="Серия"
                register={register}
                value={values.series}
              />
            </FieldsCell>
            <FieldsCell lg={2}>
              <CustomField
                id="number"
                name="number"
                label="Номер"
                register={register}
                value={values.number}
              />
            </FieldsCell>
            <FieldsCell lg={2}>
              <CustomField
                id="issueDate"
                name="issueDate"
                label="Дата выдачи"
                register={register}
                value={values.issueDate}
              />
            </FieldsCell>
            <FieldsCell lg={3}>
              <CustomField
                id="departmentCode"
                name="departmentCode"
                label="Код подразделения"
                register={register}
                value={values.departmentCode}
              />
            </FieldsCell>
          </FieldsRow>
          <HorisontalSpacer />
          <FieldsRow>
            <FieldsCell lg={12}>
              <CustomField
                id="issuedBy"
                name="issuedBy"
                label="Кем выдан"
                component="textarea"
                register={register}
                value={values.issuedBy}
              />
            </FieldsCell>
          </FieldsRow>
          <HorisontalSpacer />
          <FieldsRow>
            <FieldsCell lg={12}>
              <CustomField
                id="birthPlace"
                name="birthPlace"
                label="Место рождения"
                register={register}
                value={values.birthPlace}
              />
            </FieldsCell>
          </FieldsRow>
          <HorisontalSpacer />
          <FieldsRow>
            <FieldsCell lg={12}>
              <CustomField
                id="registrationAddress"
                name="registrationAddress"
                label="Адрес регистрации"
                register={register}
                value={values.registrationAddress}
              />
            </FieldsCell>
          </FieldsRow>
          <HorisontalSpacer />
        </FieldsToggleBlock>
      </FieldsBlock>
    </form>
  );
};

export default PassportForm;

PassportForm.propTypes = {
  values: PropTypes.objectOf(PropTypes.any).isRequired,
  onSubmit: PropTypes.func.isRequired,
  isOpened: PropTypes.bool,
  formID: PropTypes.string.isRequired,
  toggleOpened: PropTypes.func.isRequired,
};

PassportForm.defaultProps = {
  isOpened: false,
};
