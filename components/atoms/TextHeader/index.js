import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';

const TextHeaderStyled = styled.div`
  font-family: "MTSSans", sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 1.5rem;
  line-height: 2rem;
  letter-spacing: -.5px;
  font-feature-settings: 'tnum' on, 'lnum' on;
  color: ${palette.textBlack};
`;

export default function TextHeader({ header }) {
  return (
    <TextHeaderStyled>
      {header}
    </TextHeaderStyled>
  );
}

TextHeader.propTypes = {
  header: PropTypes.string.isRequired,
};
