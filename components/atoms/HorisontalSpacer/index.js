import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const HorizontalSpacerStyled = styled.div`
  display: block;
`;

const HorizontalSpacer = ({ height }) => (
  <HorizontalSpacerStyled style={{ height }} />
);

HorizontalSpacer.propTypes = {
  height: PropTypes.string,
};

HorizontalSpacer.defaultProps = {
  height: '1.5rem',
};

export default HorizontalSpacer;
