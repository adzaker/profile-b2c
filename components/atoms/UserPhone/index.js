import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import phoneReplacer from '../../../constants/phoneReplacer';

const UserPhoneStyled = styled.span`

`;

const UserPhone = ({ phone, isEmail }) => (
  <UserPhoneStyled>
    {phoneReplacer(phone)}
    {isEmail ? ' · ' : ''}
  </UserPhoneStyled>
);


UserPhone.propTypes = {
  phone: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isEmail: PropTypes.bool,
};

UserPhone.defaultProps = {
  phone: null,
  isEmail: null,
};


export default UserPhone;
