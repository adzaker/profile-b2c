import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import Icon from '../Icon';

const ControlButtonStyled = styled.button`
  display: inline;
  border: none;
  padding: 0;
  margin: 0;
  cursor: pointer;
  color: ${palette.blue};
  font-family: "MTSSans", sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  background-color: transparent;
  font-feature-settings: 'tnum' on, 'lnum' on;
  margin-right: .5rem;
`;

const StyledIcon = styled.span`
  margin-left: .25rem;
  margin-right: .25rem;
`;

const ControlButton = ({
  icon, name, onClick, id,
}) => (
  <ControlButtonStyled type="button" onClick={onClick} id={id}>
    {icon && (
    <StyledIcon>
      <Icon name={icon} color={palette.blue} />
    </StyledIcon>
    )}
    {name}
  </ControlButtonStyled>
);


ControlButton.propTypes = {
  icon: PropTypes.string,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

ControlButton.defaultProps = {
  icon: null,
};

export default ControlButton;
