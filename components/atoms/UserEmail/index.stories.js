import React from 'react';
import UserEmail from './index';

export default {
  component: UserEmail,
  title: 'Atoms|UserEmail',
  excludeStories: /.*data$/,
};

export const Default = () => <UserEmail email="default@mail.ru" />;
