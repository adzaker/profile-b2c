import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import data from './imgs/data';
import palette from '../../palette';

const IconStyled = styled.div`
  display: inline-block;
  font-size: 0;

  svg {
    fill: currentColor;
  }
`;

const Icon = ({ name = 'changeFieldIcon', color = palette.textBlack }) => (
  <IconStyled style={{ color }} dangerouslySetInnerHTML={{ __html: require(`./imgs/${name}.svg?include`) }} />
);

export default Icon;

Icon.propTypes = {
  name: PropTypes.oneOf(data).isRequired,
  color: PropTypes.string,
};

Icon.defaultProps = {
  color: '#0000000',
};
