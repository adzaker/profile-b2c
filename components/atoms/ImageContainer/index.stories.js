import React from 'react';
import ImageContainer from './index';

export default {
  component: ImageContainer,
  title: 'Atoms|ImageContainer',
  excludeStories: /.*data$/,
};

export const arrowRight = () => <ImageContainer icon="arrowRight" color="#000000" active />;
