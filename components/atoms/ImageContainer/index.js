import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import Icon from '../Icon';


const ImageContainerStyled = styled.div`
    position: absolute;
    width: 1.5rem;
    height: 1.5rem;
    display: flex;
    justify-content: center;
    align-items: center;
    left: 1.5rem;
`;

const ImageContainer = ({
  active, icon, color,
}) => (
  <ImageContainerStyled>
    <Icon name={icon} color={color || (active ? palette.redMts : palette.textBlack)} />
  </ImageContainerStyled>
);

export default ImageContainer;

ImageContainer.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  active: PropTypes.bool,
};

ImageContainer.defaultProps = {
  icon: null,
  color: null,
  active: false,
};
