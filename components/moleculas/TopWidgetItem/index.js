import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import ImageContainer from '../../atoms/ImageContainer';

const TopWidgetItemStyled = styled.li`
  padding: 0;
  height: 3rem;
  position: relative;
  
  a {
    font-family: "MTSSans", sans-serif;
    font-size: 1rem;
    line-height: 1.5;
    padding-left: 4.25rem;
    height: 100%;
    display: flex;
    align-items: center;
    position: relative;
    cursor: pointer;
    text-decoration: none;
    color: ${palette.textBlack};
    transition: color .12s ease;

    &:hover {
      color: ${palette.redMts};
    }

    &:active {
      color: ${palette.redDark};
    }
  }
`;


const TopWidgetItem = ({
  label, id, icon, url,
}) => (
  <TopWidgetItemStyled id={id}>
    <a href={url}>
      <ImageContainer icon={icon} label={label} color={palette.gray} />
      <span>{label}</span>
    </a>
  </TopWidgetItemStyled>
);

export default TopWidgetItem;


TopWidgetItem.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  icon: PropTypes.string,
  url: PropTypes.string.isRequired,
};

TopWidgetItem.defaultProps = {
  label: null,
  icon: null,
};
