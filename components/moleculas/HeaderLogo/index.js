import React from 'react';
import styled from 'styled-components';

const HeaderLogoStyled = styled.div`
    display: flex;
  `;


export default function HeaderLogo() {
  return (
    <HeaderLogoStyled>
      <img src="/images/headerLogo.svg" alt="" />
    </HeaderLogoStyled>
  );
}
