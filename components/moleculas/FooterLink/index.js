import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Icon from '../../atoms/Icon';
import palette from '../../palette';

const FooterLinkStyled = styled.a`
  font-family: "MTSSans", sans-serif;
  font-size: 1rem;
  line-height: 1.5;
  font-feature-settings: 'tnum' on, 'lnum' on;
  color: ${palette.blue};
  text-decoration: none;
  display: inline-flex;
  align-items: center;
  margin-left: 2.5rem;
  position: relative;
  
  &::after {
    content: '';
    height: 1px;
    background-color: ${palette.blue};
    width: calc(100% - 1.25rem);
    position: absolute;
    bottom: 0;
    opacity: 0;
    transform: translateY(-2px);
    transition: all .12s ease;
  }
  
  &:hover {
    &::after {
      opacity: 1;
      transform: translateY(0);
    }
  }
  
  svg {
    padding-left: .5rem;
  }
`;


const FooterLink = ({ linkName, linkUrl }) => (
  <FooterLinkStyled href={linkUrl}>
    {linkName}
    <Icon name="arrowRight" />
  </FooterLinkStyled>
);

FooterLink.propTypes = {
  linkName: PropTypes.string.isRequired,
  linkUrl: PropTypes.string.isRequired,
};

export default FooterLink;
