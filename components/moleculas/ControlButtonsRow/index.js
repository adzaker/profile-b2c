import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ControlButton from '../../atoms/ControlButton';


const ControlButtonsRowStyled = styled.div`
  display: flex;
  flex-flow: row nowrap;
  width: auto;
  padding-left: 1rem;
`;

const ControlButtonsRow = ({ controlButtons, id, changeReadable }) => {
  if (controlButtons) {
    return (
      <ControlButtonsRowStyled>
        {controlButtons.map((button) => {
          let buttonName;
          let buttonFunction;
          let buttonIcon;
          switch (button) {
            case 'delete':
              buttonName = 'Удалить';
              break;
            case 'change':
              buttonName = 'Редактировать';
              buttonFunction = changeReadable;
              buttonIcon = 'changeFieldIcon';
              break;
            default:
              break;
          }
          return (
            <ControlButton
              name={`${buttonName}`}
              type="button"
              onClick={buttonFunction}
              icon={buttonIcon}
              id={`${id}.${button}`}
              key={`${id}.${button}`}
            />
          );
        })}
      </ControlButtonsRowStyled>
    );
  }
  return '';
};

ControlButtonsRow.propTypes = {
  controlButtons: PropTypes.arrayOf(PropTypes.any),
};

ControlButtonsRow.defaultProps = {
  controlButtons: null,
};


export default ControlButtonsRow;
