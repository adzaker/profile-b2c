import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import FooterLink from '../FooterLink';

const FooterLinksStyled = styled.span`
  margin-left: auto;
`;


const FooterLinks = ({ links }) => (
  <FooterLinksStyled>
    {links.map((link) => (
      <FooterLink
        linkName={link.name}
        linkUrl={link.url}
        key={link.name}
      />
    ))}
  </FooterLinksStyled>
);

FooterLinks.propTypes = {
  links: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default FooterLinks;
