import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

export const TopWidgetImageStyled = styled.div`
  width: 2.5rem;
  height: 2.5rem;
  overflow: hidden;
  border-radius: 50%;
`;

const TopWidgetUserImg = styled.img`

`;

const TopWidgetImage = ({
  image, name, gender,
}) => {
  const defaultImage = gender === 'Мужской'
    ? '/images/defaultUserPhoto.svg'
    : '/images/defaultUserPhoto-female.svg';
  return (
    <TopWidgetImageStyled>
      <TopWidgetUserImg src={image || defaultImage} alt={name} />
    </TopWidgetImageStyled>
  );
};

export default TopWidgetImage;


TopWidgetImage.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  gender: PropTypes.string,
};

TopWidgetImage.defaultProps = {
  image: null,
  name: null,
  gender: 'Мужской',
};
