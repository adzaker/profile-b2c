import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import ImageContainer from '../../atoms/ImageContainer';

const MenuItemStyled = styled.li`
  display: flex;
  padding: 0;
  height: 3.25rem;
  align-items: center;
  position: relative;
  cursor: pointer;
  
  &.-active {
    color: ${palette.redMts};
    
    span {
      font-weight: 500;
    }
  }
  
  span {
    font-family: "MTSSans", sans-serif;
    font-size: 1rem;
    line-height: 1.5;
    padding-left: 3.5rem;
  }
`;

const MenuItem = ({
  label, id, icon, active, scrollToBlock, scrollTo, index,
}) => (
  <MenuItemStyled
    onClick={scrollToBlock}
    className={`menuItem ${active ? '-active' : ''}`}
    id={id}
    data-scroll={scrollTo}
    data-index={index}
  >
    <ImageContainer
      active={active}
      color={active ? palette.redMts : palette.textBlack}
      icon={icon}
      label={label}
    />
    <span>{label}</span>
  </MenuItemStyled>
);

export default MenuItem;


MenuItem.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  icon: PropTypes.string,
  scrollTo: PropTypes.string.isRequired,
  active: PropTypes.bool,
  index: PropTypes.number.isRequired,
  scrollToBlock: PropTypes.func.isRequired,
};

MenuItem.defaultProps = {
  label: null,
  icon: null,
  active: false,
};
