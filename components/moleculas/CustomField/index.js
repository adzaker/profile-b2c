import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ControlButtonsRow from '../ControlButtonsRow';
import palette from '../../palette';

const ImageContainer = styled.div`
  font-size: 0;
  width: 2.5rem;
  height: 2.5rem;
  margin-right: 1.25rem;
  border-radius: 50%;
  overflow: hidden;
  
  img {
    width: 100%;
    height: auto;
  }
`;

const FieldContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const FieldContainerTop = styled.div`
  display: flex;
    padding-bottom: .25rem;
`;

const CustomFieldStyled = styled.div`
  display: flex;
  font-family: "MTSSans", sans-serif;
  font-feature-settings: 'tnum' on, 'lnum' on;
  
  label {
    font-weight: 400;
    font-size: .875rem;
    line-height: 1.25rem;
    color: ${palette.grayRaven};
  }
`;

const FieldInput = styled.input`
  font-size: 1rem;
  line-height: 1.5rem;
  width: 100%;
  font-family: "MTSSans", sans-serif;
  border: 2px solid ${palette.grayUltraLight};
  border-radius: .25rem;
  height: 3.25rem;
  padding: 0 1.25rem;
  box-sizing: border-box;
  transition: all .2s ease;

  &[readOnly] {
    border-color: transparent;
    border-width: 0;
    outline: none;
    cursor: default;
    padding: 0;
    height: 2rem;
  }
`;

const FieldTextarea = styled.textarea`
  font-size: 1rem;
  line-height: 1.5rem;
  width: 100%;
  padding: 0;
  font-family: "MTSSans", sans-serif;
  resize: none;

  &[readOnly] {
    border: none;
    outline: none;
    cursor: default;
  }
`;

const FieldHiddenText = styled.span`
  overflow: hidden;
  height: 0;
  padding: 0 2.5rem;
  display: block;
`;

const CustomField = ({
  image, label, id, name, component, value, register, buttons,
}) => {
  const [isReadOnly, setReadOnly] = useState(true);

  const changeReadable = (e) => {
    if (e.target.id === `${id}.change`) {
      document.getElementById(id).focus();
    }
    setReadOnly(!isReadOnly);
  };

  const setUnreadable = () => {
    setReadOnly(true);
  };

  const checkEnterButton = (e) => {
    if (e.key === 'Enter') setUnreadable();
  };

  return (
    <CustomFieldStyled>
      {image && (
        <ImageContainer>
          <img
            src={image}
            alt={label}
          />
        </ImageContainer>
      )}
      <FieldContainer>
        <FieldContainerTop>
          <label htmlFor={id}>{label}</label>
          <ControlButtonsRow
            controlButtons={buttons}
            id={id}
            changeReadable={changeReadable}
          />
        </FieldContainerTop>
        <FieldHiddenText>{value}</FieldHiddenText>
        {component === 'textarea'
          ? (
            <FieldTextarea
              name={name}
              id={id}
              ref={register}
              value={value}
              component={component}
              placeholder="Не заполнено"
              readOnly={isReadOnly}
              onBlur={setUnreadable}
              onKeyPress={checkEnterButton}
            />
          )
          : (
            <FieldInput
              name={name}
              id={id}
              ref={register}
              value={value}
              component={component}
              placeholder="Не заполнено"
              readOnly={isReadOnly}
              onBlur={setUnreadable}
              onKeyPress={checkEnterButton}
            />
          )}
      </FieldContainer>
    </CustomFieldStyled>
  );
};


export default CustomField;


CustomField.propTypes = {
  image: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  component: PropTypes.string,
  value: PropTypes.string,
  register: PropTypes.func.isRequired,
  buttons: PropTypes.arrayOf(PropTypes.any),
};

CustomField.defaultProps = {
  image: null,
  label: null,
  value: null,
  buttons: [],
  component: 'input',
};
