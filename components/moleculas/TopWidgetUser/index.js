import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import palette from '../../palette';
import TopWidgetImage from '../TopWidgetImage';

const TopWidgetUserStyled = styled.div`
  padding: 1rem 0 .75rem 1.25rem;
  height: 4.25rem;
  position: relative;
  display: flex;
  align-items: center;
  box-sizing: border-box;
`;

const TopWidgetUserInfo = styled.div`
  height: 2.5rem;
  display: flex;
  flex-direction: column;
  padding-left: .75rem;
  font-family: 'MTSSans', sans-serif;
`;

const PrimaryLine = styled.span`
  font-size: 1rem;
  font-weight: 500;
  line-height: 1.5;
  color: ${palette.textBlack}
`;

const SecondaryLine = styled.span`
  font-size: .875rem;
  font-weight: 400;
  line-height: 1.21;
  color: ${palette.grayRaven}
`;


const TopWidgetUser = ({
  image, name, phone, gender, owner,
}) => (
  <TopWidgetUserStyled>
    <TopWidgetImage
      image={image}
      name={name}
      gender={gender}
    />
    <TopWidgetUserInfo>
      <PrimaryLine>
        {name || phone}
      </PrimaryLine>
      <SecondaryLine>
        {name
          ? phone
          : owner || ''}
      </SecondaryLine>
    </TopWidgetUserInfo>
  </TopWidgetUserStyled>
);

export default TopWidgetUser;


TopWidgetUser.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  phone: PropTypes.string.isRequired,
  gender: PropTypes.string,
  owner: PropTypes.string,
};

TopWidgetUser.defaultProps = {
  image: '/images/defaultUserPhoto.svg',
  name: null,
  gender: null,
  owner: null,
};
