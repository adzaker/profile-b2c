import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const FieldsRowStyled = styled.div`
    display: flex;
    padding: 0 2rem;
    justify-content: flex-start;
    margin: 0 -1rem;
  `;


export default function FieldsRow({ isWrap = false, children }) {
  return (
    <FieldsRowStyled
      style={{ flexWrap: `${isWrap ? 'wrap' : 'nowrap'}` }}
    >
      {children}
    </FieldsRowStyled>
  );
}

FieldsRow.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.any)]).isRequired,
  isWrap: PropTypes.bool,
};

FieldsRow.defaultProps = {
  isWrap: false,
};
