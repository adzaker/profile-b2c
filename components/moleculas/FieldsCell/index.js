import React from 'react';
import PropTypes from 'prop-types';
import breakpoints from '../../../constants/breakpoints';

export default function FieldsCell({
  lg, md, sm, xs, children,
}) {
  return (
    <>
      <style jsx>
        {`
          div {
            padding: 0 1rem;
            box-sizing: border-box;
          }
          @media (min-width: ${breakpoints.lg + 1}px) {
            div {
              flex: 0 1 ${(lg ? (lg / 12) : 1) * 100}%;
            }
          }
          
          @media (max-width: ${breakpoints.md}px) {
            div {
              flex: 0 1 ${(md ? (md / 12) : (lg / 12)) * 100}%;
            }
          }
          
          @media (max-width: ${breakpoints.sm}px) {
            div {
              flex: 0 1 ${(sm ? (sm / 12) : md ? (md / 12) : (lg / 12)) * 100}%;
            }
          }
          
          @media (max-width: ${breakpoints.xs}px) {
            div {
              flex: 0 1 ${(xs ? (xs / 12) : sm ? (sm / 12) : md ? (md / 12) : (lg / 12)) * 100}%;
            }
          }
        `}
      </style>
      <div>
        {children}
      </div>
    </>
  );
}

FieldsCell.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.objectOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.any)]).isRequired,
  lg: PropTypes.number,
  md: PropTypes.number,
  sm: PropTypes.number,
  xs: PropTypes.number,
};

FieldsCell.defaultProps = {
  lg: null,
  md: null,
  sm: null,
  xs: null,
};
