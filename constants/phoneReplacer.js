export default (phone) => {
  const replacer4 = (str, p1, p2, p3, p4) => `+7 ${p1} ${p2}-${p3}-${p4}`;
  const threeNums = '(\\d{3})';
  const twoNums = '(\\d{2})';
  return phone.replace(new RegExp(threeNums + threeNums + twoNums + twoNums), replacer4);
};
